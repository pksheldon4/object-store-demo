package com.pksheldon4.objectstore.endpoint;

import com.pksheldon4.objectstore.config.MinioProperties;
import com.pksheldon4.objectstore.model.UploadMetaData;
import com.pksheldon4.objectstore.model.UploadResult;
import com.pksheldon4.objectstore.repository.UploadMetaDataRepository;
import io.minio.BucketExistsArgs;
import io.minio.ListObjectsArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.Result;
import io.minio.messages.Item;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
public class SampleController {

    private final MinioClient minioClient;
    private final MinioProperties minioProperties;
    private final UploadMetaDataRepository metaDataRepository;


    public SampleController(UploadMetaDataRepository repository, MinioClient client, MinioProperties properties) {
        this.metaDataRepository = repository;
        this.minioClient = client;
        this.minioProperties = properties;
    }


    @PostMapping(value = "/test", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void test(@RequestPart("file") MultipartFile file) {

        String name = file.getOriginalFilename();
        if (!file.isEmpty()) {
            try {
                log.info("You successfully uploaded file=" + name);
            } catch (Exception e) {
                log.error("You failed to upload " + name + " => " + e.getMessage());
            }
        } else {
            log.error("You failed to upload " + name
                + " because the file was empty.");
        }
    }

    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void uploadFileHandler(@RequestPart("metadata") UploadMetaData metadata,
                                  @RequestPart("file") MultipartFile file) {

        String name = file.getOriginalFilename();
        if (!file.isEmpty()) {
            try {
                //Store file
                storeObject(name, new ByteArrayInputStream(file.getBytes()));
                //save metadata
                metaDataRepository.save(metadata);
                log.info("You successfully uploaded file=" + name);
            } catch (Exception e) {
                log.error("You failed to upload " + name + " => " + e.getMessage());
            }
        } else {
            log.error("You failed to upload " + name
                + " because the file was empty.");
        }
    }

    @GetMapping("/uploads")
    public List<UploadResult> listUploads() throws Exception {
        List<UploadResult> results = new ArrayList<>();
        //If configured Bucket exists, list the contents
        if (bucketExists(minioProperties.getBucket())) {
            for (Result<Item> itemResult : minioClient.listObjects(ListObjectsArgs.builder().bucket(minioProperties.getBucket()).build())) {
                Item item = itemResult.get();
                results.add(new UploadResult(item.objectName(), item.size()));
            }
        }
        return results;
    }

    @GetMapping("/metadata")
    public List<UploadMetaData> listMetadata() {
        return metaDataRepository.findAll();
    }

    public void storeObject(String name, InputStream inputStream) throws Exception {
        makeDefaultBucketIfNotExists();
        minioClient.putObject(
            PutObjectArgs.builder().bucket(minioProperties.getBucket()).object(name).stream(
                inputStream, inputStream.available(), -1)
                .build());
    }

    private void makeDefaultBucketIfNotExists() throws Exception {
        if (!bucketExists(minioProperties.getBucket())) {
            minioClient.makeBucket(MakeBucketArgs.builder().bucket(minioProperties.getBucket()).build());
        }
    }

    private boolean bucketExists(String bucketName) throws Exception {
        return minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
    }
}
