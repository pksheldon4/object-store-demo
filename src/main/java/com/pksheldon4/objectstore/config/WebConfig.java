package com.pksheldon4.objectstore.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Collections;
import java.util.List;

/**
 * For multipart/form data submitted through Swagger, the individual Part's do not get assigned their pwn ContentType.
 * Spring defaults the content-type to `application/octet-stream` in `AbstractMessageConverterMethodArgumentResolver.readWithMessageConverters(...)`
 * which isn't handled by any of the default message converters.
 * <p>
 * This configuration creates an extended version of the MappingJackson2HttpMessageConverter converter and sets it to handle `application/octet-stream`
 * payloads.  The data still is required to be valid JSON.
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {

        converters.add(new OctetStreamMappingJackson2HttpMessageConverter());
    }

    public static class OctetStreamMappingJackson2HttpMessageConverter extends MappingJackson2HttpMessageConverter {

        private static final List<MediaType> supportedMediaTypes = Collections.singletonList(MediaType.APPLICATION_OCTET_STREAM);

        @Override
        public List<MediaType> getSupportedMediaTypes() {
            return supportedMediaTypes;
        }
    }
}

