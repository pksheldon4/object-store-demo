package com.pksheldon4.objectstore.repository;

import com.pksheldon4.objectstore.model.UploadMetaData;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UploadMetaDataRepository extends MongoRepository<UploadMetaData, String> {
}
