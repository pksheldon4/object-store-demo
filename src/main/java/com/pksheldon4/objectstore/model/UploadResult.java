package com.pksheldon4.objectstore.model;

import lombok.Getter;

@Getter
public class UploadResult {
    private final String fileName;
    private final long fileSize;

    public UploadResult(String filename, long fileSize) {
        this.fileName = filename;
        this.fileSize = fileSize;
    }
}
