package com.pksheldon4.objectstore.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class UploadMetaData {

    @Id
    private String id;
    private String firstName;
    private String lastName;

}
