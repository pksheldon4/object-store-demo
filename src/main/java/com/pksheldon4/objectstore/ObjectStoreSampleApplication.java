package com.pksheldon4.objectstore;

import com.pksheldon4.objectstore.config.MinioProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories(basePackages = "com.pksheldon4.objectstore.repository")
@EnableConfigurationProperties(MinioProperties.class)
public class ObjectStoreSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ObjectStoreSampleApplication.class, args);
    }

}
